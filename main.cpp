#include <iostream>
#include <vector>
#include <string>
#include "glut.h"

void cb_display(){
	glClear(GL_COLOR_BUFFER_BIT);
	glutSwapBuffers();
}

int main(int argc, char** argv){
	glutInit(&argc, argv);
	
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);
	glutInitWindowSize(500, 500);
	glutCreateWindow("golf");

	glutDisplayFunc(cb_display);

	glutMainLoop();
//	system("pause");
	return 0;
};